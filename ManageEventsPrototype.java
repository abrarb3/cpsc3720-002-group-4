import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;

@SuppressWarnings("serial")
public class ManageEventsPrototype extends JFrame {

	private int currentI;
	private JScrollPane heatPane;
	private JTable issueTable;
	private DefaultTableModel modelIssues;
	private JPanel contentPane;
	private JTextField textFieldID;
	private JTextField textFieldTitle;
	private JTextField textFieldDescription;
	private JComboBox comboBoxStatus;
	private JPanel buttonPanel;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ManageEventsPrototype frame = new ManageEventsPrototype();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ManageEventsPrototype() {
		setTitle("Project Management System");
		setAlwaysOnTop(true);
		JFrame frame = new JFrame("Project Management System");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 670, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(1, 0, 1, 2));
		setContentPane(contentPane);
				
		contentPane.setLayout(null);
		
		JLabel lblIssues = new JLabel("Heat Information");
		lblIssues.setHorizontalAlignment(SwingConstants.CENTER);
		lblIssues.setBounds(12, 33, 646, 27);
		lblIssues.setFont(new Font("Tahoma", Font.PLAIN, 12));
		contentPane.add(lblIssues);		
		
		DefaultTableModel modelProjects = new DefaultTableModel(new Object[][] {}, new Object[] {"Project Names"});
		Object[] projObj1= {"Project One"};
		modelProjects.addRow(projObj1);
		Object[] projObj2= {"Project Two"};
		modelProjects.addRow(projObj2);
		Object[] projObj3= {"Project Three"};
		modelProjects.addRow(projObj3);
		
		heatPane = new JScrollPane();
		heatPane.setBounds(12, 66, 646, 136);
		contentPane.add(heatPane);
		
		modelIssues = new DefaultTableModel(new Object[][] {}, new Object[] {"Code", "Name", "Gender", "MinAge", "MaxAge", "Time", "NumHeats"});
		
		issueTable = new JTable(modelIssues);
		issueTable.setEnabled(false);
		heatPane.setViewportView(issueTable);
		issueTable.setBorder(new LineBorder(new Color(0, 0, 0)));
		Object[] fooInfo = {"E001","50m Dash", "Female","8", "11", "10:45:00","2"};
		Object[] fooInfo2 = {"E001","50m Dash", "Female","12", "13", "11:40:00","1"};
		modelIssues.addRow(fooInfo);
		modelIssues.addRow(fooInfo2);
		
		buttonPanel = new JPanel();
		buttonPanel.setBounds(22, 203, 636, 27);
		contentPane.add(buttonPanel);
		buttonPanel.setLayout(null);
		
		JButton btnNewButton = new JButton("Create");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//stuff
			}
		});
		btnNewButton.setBounds(44, 0, 89, 23);
		buttonPanel.add(btnNewButton);
		
		JButton btnEdit = new JButton("Edit");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		btnEdit.setBounds(269, 0, 89, 23);
		buttonPanel.add(btnEdit);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (issueTable.getSelectedRow() != -1){
					modelIssues.removeRow(issueTable.getSelectedRow());
				}
			}
		});
		btnDelete.setBounds(489, 0, 89, 23);
		buttonPanel.add(btnDelete);
		
		JPanel dataPanel = new JPanel();
		dataPanel.setBounds(12, 232, 646, 156);
		contentPane.add(dataPanel);
		dataPanel.setLayout(null);
		
		JLabel lblId = new JLabel("Code");
		lblId.setBounds(427, 39, 46, 14);
		dataPanel.add(lblId);
		
		JLabel lblTitle = new JLabel("Event Name");
		lblTitle.setBounds(10, 39, 93, 14);
		dataPanel.add(lblTitle);
		
		JLabel lblDescription = new JLabel("Min Age");
		lblDescription.setBounds(10, 73, 69, 14);
		dataPanel.add(lblDescription);
		
		JLabel lblStatus = new JLabel("Gender");
		lblStatus.setBounds(427, 73, 61, 14);
		dataPanel.add(lblStatus);
		
		textFieldID = new JTextField();
		textFieldID.setBounds(490, 37, 85, 20);
		dataPanel.add(textFieldID);
		textFieldID.setColumns(10);
		
		textFieldTitle = new JTextField();
		textFieldTitle.setBounds(103, 37, 306, 20);
		dataPanel.add(textFieldTitle);
		textFieldTitle.setColumns(10);
		
		textFieldDescription = new JTextField();
		textFieldDescription.setBounds(103, 71, 85, 20);
		dataPanel.add(textFieldDescription);
		textFieldDescription.setColumns(10);
		
		comboBoxStatus = new JComboBox();
		comboBoxStatus.setMaximumRowCount(2);
		comboBoxStatus.setModel(new DefaultComboBoxModel(new String[] {"Male", "Female"}));
		comboBoxStatus.setBounds(490, 70, 85, 20);
		dataPanel.add(comboBoxStatus);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(292, 69, 85, 20);
		dataPanel.add(textField);
		
		JLabel lblMaxAge = new JLabel("Max Age");
		lblMaxAge.setBounds(206, 73, 81, 14);
		dataPanel.add(lblMaxAge);
		
		JLabel lblTime = new JLabel("Time");
		lblTime.setBounds(10, 107, 69, 14);
		dataPanel.add(lblTime);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(103, 105, 85, 20);
		dataPanel.add(textField_1);
		
		JLabel lblNumHeats = new JLabel("Num Heats");
		lblNumHeats.setBounds(206, 107, 81, 14);
		dataPanel.add(lblNumHeats);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(292, 101, 85, 20);
		dataPanel.add(textField_2);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 670, 21);
		contentPane.add(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Manage Events");
		mnFile.add(mntmNewMenuItem);
		
		JMenuItem mntmManageHeats = new JMenuItem("Manage Heats");
		mnFile.add(mntmManageHeats);
		
	}
}	
