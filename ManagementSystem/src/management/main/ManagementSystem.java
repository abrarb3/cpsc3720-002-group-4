package management.main;
/**
 * This is the main class that puts everything together for the user to run
 * @param String[] args
 * @exception None
 * @return None
 */


import management.controller.*;
import management.ui.ManagementFrame;

public class ManagementSystem {

	public static void main(String[] args) {
		System.out.println("Loading...");
		GenerateHeatSheetsController ghCtrl = new GenerateHeatSheetsController();
		AthleteViewController avc = new AthleteViewController();
		EventViewController evc = new EventViewController();
		HeatViewController hvc = new HeatViewController();
		GroupLeaderViewController gvc = new GroupLeaderViewController();
		SchoolViewController svc = new SchoolViewController();
		System.out.println("Done loading database!");
		ManagementFrame mFrame = new ManagementFrame(ghCtrl, avc, evc, hvc, gvc, svc);
		mFrame.setVisible(true);
	}

}
