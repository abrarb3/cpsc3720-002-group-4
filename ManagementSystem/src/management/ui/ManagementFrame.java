package management.ui;

import java.awt.*;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import management.controller.*;

@SuppressWarnings("serial")
public class ManagementFrame extends JFrame {
	
	public ManagementFrame(GenerateHeatSheetsController ghCtrl, AthleteViewController avc,  EventViewController evc, HeatViewController hvc, GroupLeaderViewController gvc, SchoolViewController svc) {
		
		getContentPane().setForeground(Color.WHITE);
		JFrame ManagementFrame = new JFrame("Athlete Management System");
		ManagementFrame.setTitle("Athlete Management System");
		setSize(800,600);
		getContentPane().setLayout(null);
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 800, 600);
		
		HomePanel homePanel = new HomePanel();
		tabbedPane.addTab("Home", null, homePanel, null);
		
		AthletePanel athletePanel = new AthletePanel(avc);
		tabbedPane.addTab("Athletes", null, athletePanel, null);
		
		GroupLeaderPanel groupLeaderPanel = new GroupLeaderPanel(gvc);
		tabbedPane.addTab("Users", null, groupLeaderPanel, null);
		
		SchoolPanel schoolPanel = new SchoolPanel(svc);
		tabbedPane.addTab("Schools", null, schoolPanel, null);
		EventPanel eventPanel = new EventPanel(evc);
		tabbedPane.addTab("Events", null, eventPanel, null);
		HeatsPanel heatPanel = new HeatsPanel(ghCtrl, hvc);
		tabbedPane.addTab("Heats", null, heatPanel, null);
		getContentPane().add(tabbedPane);

		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
