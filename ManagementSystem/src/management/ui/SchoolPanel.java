package management.ui;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;
import management.controller.SchoolViewController;

@SuppressWarnings("serial")
public class SchoolPanel extends JPanel {
	private JTable table;
	private JTextField textFieldName = new JTextField();
	JComboBox<String> comboBoxEventName1 = new JComboBox<String>();
	JComboBox<String> comboBoxGender = new JComboBox<String>();
	JComboBox<String> comboBoxEventName2 = new JComboBox<String>();
	private JTextField textFieldGroupCode;

	public SchoolPanel(SchoolViewController svc) {
		setForeground(Color.WHITE);
		setLayout(null);
		setSize(800,590);
		JLabel labelSchoolPanelTitle = new JLabel("School Information");
		labelSchoolPanelTitle.setBounds(331, 12, 138, 15);
		add(labelSchoolPanelTitle);
		
		JButton buttonCreate = new JButton("Create");
		buttonCreate.setToolTipText("Creates a new athlete based on parameters below");
		buttonCreate.setBounds(110, 360, 117, 25);
		add(buttonCreate);
		
		JButton buttonEdit = new JButton("Edit");
		buttonEdit.setToolTipText("Edits the currently selected athlete");
		buttonEdit.setBounds(339, 360, 117, 25);
		add(buttonEdit);
		
		JButton buttonDelete = new JButton("Delete");
		buttonDelete.setToolTipText("Deletes the current selected athlete");
		buttonDelete.setBounds(568, 360, 117, 25);
		add(buttonDelete);
		
		JLabel labelName = new JLabel("School Name");
		labelName.setBounds(46, 412, 100, 15);
		add(labelName);
		
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(21, 39, 757, 300);
		add(scrollPane);
		
		DefaultTableModel modelSchool = new DefaultTableModel(new Object[][] {}, new Object[] {"Group Code", "School Name"});
		table = new JTable(modelSchool);
		table.setBorder(new LineBorder(new Color(0, 0, 0)));
		table.setBounds(252, 138, 300, 300);
		scrollPane.setViewportView(table);
		svc.generateGTable(modelSchool);
		
		textFieldName.setToolTipText("*Required Field - Please enter the name of the school");
		textFieldName.setColumns(10);
		textFieldName.setBounds(189, 410, 178, 19);
		add(textFieldName);
		
		JLabel lblGroupCode = new JLabel("Group Code");
		lblGroupCode.setBounds(46, 469, 100, 15);
		add(lblGroupCode);
		
		textFieldGroupCode = new JTextField();
		textFieldGroupCode.setToolTipText("*Required Field - Please enter the name of the group leader");
		textFieldGroupCode.setColumns(10);
		textFieldGroupCode.setBounds(189, 467, 178, 19);
		add(textFieldGroupCode);
	}
}
