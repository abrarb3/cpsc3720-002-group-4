
package management.ui;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.border.LineBorder;
import management.controller.GroupLeaderViewController;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class GroupLeaderPanel extends JPanel {
	private JTable table;
	private JTextField textFieldName = new JTextField();
	JComboBox<String> comboBoxEventName1 = new JComboBox<String>();
	JComboBox<String> comboBoxGender = new JComboBox<String>();
	JComboBox<String> comboBoxEventName2 = new JComboBox<String>();
	private JTextField textFieldGroupCode;

	public GroupLeaderPanel(GroupLeaderViewController gvc) {
		setForeground(Color.WHITE);
		setLayout(null);
		setSize(800,590);
		JLabel labelGLPanelTitle = new JLabel("Group Leader Information");
		labelGLPanelTitle.setBounds(308, 12, 183, 15);
		add(labelGLPanelTitle);
		
		JButton buttonCreate = new JButton("Create");
		buttonCreate.setToolTipText("Creates a new athlete based on parameters below");
		buttonCreate.setBounds(110, 360, 117, 25);
		add(buttonCreate);
		
		JButton buttonEdit = new JButton("Edit");
		buttonEdit.setToolTipText("Edits the currently selected athlete");
		buttonEdit.setBounds(339, 360, 117, 25);
		add(buttonEdit);
		
		JButton buttonDelete = new JButton("Delete");
		buttonDelete.setToolTipText("Deletes the current selected athlete");
		buttonDelete.setBounds(568, 360, 117, 25);
		add(buttonDelete);
		
		JLabel labelName = new JLabel("Name");
		labelName.setBounds(46, 412, 81, 15);
		add(labelName);
		
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(21, 39, 757, 300);
		add(scrollPane);
		
		DefaultTableModel modelGroup = new DefaultTableModel(new Object[][] {}, new Object[] {"Group Code", "Name"});
		table = new JTable(modelGroup);
		table.setBorder(new LineBorder(new Color(0, 0, 0)));
		table.setBounds(252, 138, 300, 300);
		scrollPane.setViewportView(table);
		gvc.generateGTable(modelGroup);
		
		textFieldName.setToolTipText("*Required Field - Please enter the name of the group leader");
		textFieldName.setColumns(10);
		textFieldName.setBounds(189, 410, 178, 19);
		add(textFieldName);
		
		JLabel lblGroupCode = new JLabel("Group Code");
		lblGroupCode.setBounds(46, 469, 100, 15);
		add(lblGroupCode);
		
		textFieldGroupCode = new JTextField();
		textFieldGroupCode.setToolTipText("*Required Field - Please enter the name of the group leader");
		textFieldGroupCode.setColumns(10);
		textFieldGroupCode.setBounds(189, 467, 178, 19);
		add(textFieldGroupCode);
		
		JButton button = new JButton("Generate Group Sheets");
		button.setBounds(512, 431, 206, 25);
		add(button);

	}
}
