package management.ui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.border.LineBorder;

import management.controller.AthleteViewController;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class AthletePanel extends JPanel {
	private JTable table;
	private JTextField textFieldAge = new JTextField();
	private JTextField textFieldLastName = new JTextField();
	private JTextField textFieldFirstName = new JTextField();
	JComboBox<String> comboBoxEventName1 = new JComboBox<String>();
	JComboBox<String> comboBoxGender = new JComboBox<String>();
	JComboBox<String> comboBoxEventName2 = new JComboBox<String>();
	private JTextField textFieldScore1;
	private JTextField textFieldScore2;
	AthleteViewController a = null;

	public AthletePanel(AthleteViewController avc) {
		a = avc;
		setForeground(Color.WHITE);
		setLayout(null);
		setSize(800,590);
		JLabel labelAthletePanelTitle = new JLabel("Athlete Information");
		labelAthletePanelTitle.setBounds(331, 12, 138, 15);
		add(labelAthletePanelTitle);
		
		JButton buttonCreate = new JButton("Create");
		buttonCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					int age = Integer.parseInt(textFieldAge.getText());
					String lName = textFieldLastName.getText();
					String fName = textFieldFirstName.getText();
					String gender = null;
					if (((String)comboBoxGender.getSelectedItem()).equals("Male")){
						gender = "M";
					}
					else
						gender = "F";
					String event1 = (String) comboBoxEventName1.getSelectedItem();
					String event2 = null;				
					int score1 = Integer.parseInt(textFieldScore1.getText());
					int score2;
					if (comboBoxEventName2.getSelectedItem() != null) {
						event2 = (String) comboBoxEventName2.getSelectedItem();
						score2 = Integer.parseInt(textFieldScore2.getText());
					}
					//if(a.validateAthlete(age, lName, fName, gender, event1, event2, score1)){
						//a.createHeat(fName, lName, age, gender, event1, event2, score1, score2);
					//}
					
					/*else{
						JOptionPane.showMessageDialog(frame,
							    "Error in creating entry");
					}*/
					validate();
						repaint();
				} catch (Exception ex){
					ex.printStackTrace();
				}
		}});
		buttonCreate.setToolTipText("Creates a new athlete based on parameters below");
		buttonCreate.setBounds(110, 360, 117, 25);
		add(buttonCreate);
		
		JButton buttonEdit = new JButton("Edit");
		buttonEdit.setToolTipText("Edits the currently selected athlete");
		buttonEdit.setBounds(339, 360, 117, 25);
		add(buttonEdit);
		
		JButton buttonDelete = new JButton("Delete");
		buttonDelete.setToolTipText("Deletes the current selected athlete");
		buttonDelete.setBounds(568, 360, 117, 25);
		add(buttonDelete);
		
		JLabel labelAthleteFirstName = new JLabel("First Name");
		labelAthleteFirstName.setBounds(46, 412, 81, 15);
		add(labelAthleteFirstName);
		
		JLabel labelAge = new JLabel("Age");
		labelAge.setBounds(100, 490, 27, 15);
		add(labelAge);
		
		JLabel labelGender = new JLabel("Gender");
		labelGender.setBounds(331, 412, 63, 15);
		add(labelGender);
		
		comboBoxGender.setToolTipText("*Required Field - Please indicate the athlete's gender");
		comboBoxGender.setModel(new DefaultComboBoxModel<String>(new String[] {"Male", "Female"}));
		comboBoxGender.setBounds(444, 412, 117, 24);
		add(comboBoxGender);
		
		JLabel lblEventName1 = new JLabel("Event Name 1");
		lblEventName1.setBounds(331, 462, 95, 15);
		add(lblEventName1);
		
		JLabel lblEventName2 = new JLabel("Event Name 2");
		lblEventName2.setBounds(331, 507, 95, 15);
		add(lblEventName2);
		
		comboBoxEventName1.setToolTipText("*Required Field - Please find the event for this athlete");
		comboBoxEventName1.setBounds(444, 462, 175, 24);
		avc.generateEventsCBox(comboBoxEventName1);
		add(comboBoxEventName1);
		
		comboBoxEventName2.setToolTipText("Please indicate another event for this athlete");
		comboBoxEventName2.setBounds(444, 507, 175, 24);
		avc.generateEventsCBox(comboBoxEventName2);
		add(comboBoxEventName2);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(21, 39, 757, 300);
		add(scrollPane);
		
		DefaultTableModel modelAthletes = new DefaultTableModel(new Object[][] {}, new Object[] {"First Name", "Last Name", "Age", "Gender",  "Event Code 1", "Event Name  1", "Event Code 2", "Event Name 2",  "Score 1", "Score 2"});
		table = new JTable(modelAthletes);
		table.setBorder(new LineBorder(new Color(0, 0, 0)));
		table.getColumnModel().getColumn(2).setPreferredWidth(3);
		table.getColumnModel().getColumn(3).setPreferredWidth(15);
		table.setBounds(252, 138, 300, 300);
		scrollPane.setViewportView(table);
		avc.generateATable(modelAthletes);
		
		textFieldAge = new JTextField();
		textFieldAge.setToolTipText("*Required Field - Please enter the age of the athlete (Number 1 - 100)");
		textFieldAge.setBounds(162, 488, 114, 19);
		add(textFieldAge);
		textFieldAge.setColumns(10);
		
		JLabel labelLastName = new JLabel("Last Name");
		labelLastName.setBounds(46, 450, 81, 15);
		add(labelLastName);
		
		textFieldLastName.setToolTipText("*Required Field - Please enter the last name of the athlete");
		textFieldLastName.setColumns(10);
		textFieldLastName.setBounds(162, 448, 114, 19);
		add(textFieldLastName);
		
		textFieldFirstName.setToolTipText("*Required Field - Please enter the first name of the athlete");
		textFieldFirstName.setColumns(10);
		textFieldFirstName.setBounds(162, 410, 114, 19);
		add(textFieldFirstName);
		
		JButton btnGenerateNameTags = new JButton("Generate Name Tags");
		btnGenerateNameTags.setBounds(110, 526, 206, 25);
		add(btnGenerateNameTags);
		
		JLabel lblScores = new JLabel("Scores");
		lblScores.setBounds(671, 442, 70, 15);
		add(lblScores);
		
		textFieldScore1 = new JTextField();
		textFieldScore1.setBounds(643, 467, 114, 19);
		add(textFieldScore1);
		textFieldScore1.setColumns(10);
		
		textFieldScore2 = new JTextField();
		textFieldScore2.setColumns(10);
		textFieldScore2.setBounds(643, 512, 114, 19);
		add(textFieldScore2);
		
		JLabel lblAth = new JLabel("Athlete Name");
		lblAth.setBounds(10, 15, 81, 14);
		
		JLabel lblGender = new JLabel("Gender");
		lblGender.setBounds(10, 44, 46, 14);
		
		JLabel lblDescription = new JLabel("BirthDate");
		lblDescription.setBounds(10, 73, 81, 14);
		
		JLabel lblStatus = new JLabel("Event#");
		lblStatus.setBounds(10, 102, 81, 14);
		
		JLabel lblQsc = new JLabel("Qualifying Score");
		lblQsc.setBounds(10, 131, 106, 14);

	}
}
