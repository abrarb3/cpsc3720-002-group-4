package management.ui;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class HomePanel extends JPanel {

	public HomePanel() {
		setForeground(Color.WHITE);
		setLayout(null);
		setSize(800,590);
		JLabel labelAthletePanelTitle = new JLabel("Home Page");
		labelAthletePanelTitle.setBounds(360, 12, 80, 15);
		add(labelAthletePanelTitle);
		
		JButton btnBeginRegistration = new JButton("Begin Registration");
		btnBeginRegistration.setBounds(156, 277, 166, 36);
		add(btnBeginRegistration);
		
		JButton btnEndRegistration = new JButton("End Registration");
		btnEndRegistration.setBounds(478, 277, 166, 36);
		add(btnEndRegistration);

	}
}
