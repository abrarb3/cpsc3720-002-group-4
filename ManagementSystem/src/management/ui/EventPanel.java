package management.ui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTextArea;

import management.controller.EventViewController;

import javax.swing.DefaultComboBoxModel;

@SuppressWarnings("serial")
public class EventPanel extends JPanel {
	
	private JPanel contentPane;
	private JTable table;
	JTextArea textAreaEventName = new JTextArea();
	JTextArea textAreaEventCode = new JTextArea();
	JTextArea textAreaScoreMin = new JTextArea();
	JTextArea textAreaScoreMax = new JTextArea();
	JTextArea textAreaSortSeq = new JTextArea();
	JComboBox<String> comboBoxScoreUnit = new JComboBox<String>();
	DefaultTableModel modelEvents;
	EventViewController event = null;
	
	public EventPanel(EventViewController evc) {
		event = evc;
		setSize(800,590);
		setLayout(null);
		
		JLabel lblEventsInformation = new JLabel("Events Information");
		lblEventsInformation.setBounds(333, 12, 133, 15);
		add(lblEventsInformation);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(21, 39, 757, 300);
		add(scrollPane);
		
		modelEvents = new DefaultTableModel(new Object[][] {}, new Object[] {"Event Code", "Event Name", "Score Unit", "Score Min", "Score Max", "Sort Sequence"});
		table = new JTable(modelEvents);
		table.setBorder(new LineBorder(new Color(0, 0, 0)));
		table.setBounds(252, 138, 300, 300);
		scrollPane.setViewportView(table);
		evc.generateETable(modelEvents);
		
		JButton button = new JButton("Create");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					int sMin = Integer.parseInt(textAreaScoreMin.getText());
					int sMax = Integer.parseInt(textAreaScoreMax.getText());
					int seq = Integer.parseInt(textAreaSortSeq.getText());
					if(event.validateEvent((String)textAreaEventName.getText(), (String)textAreaEventCode.getText(), (String)comboBoxScoreUnit.getSelectedItem(), 
						sMin, sMax,seq) == true){
					event.createEvent((String)textAreaEventName.getText(), (String)textAreaEventCode.getText(), (String)comboBoxScoreUnit.getSelectedItem(), 
							sMin, sMax,seq, modelEvents);
					}
					/*else{
						JOptionPane.showMessageDialog(frame,
							    "Error in creating entry");
					}*/
					modelEvents.fireTableDataChanged();
					validate();
					repaint();
				} catch (Exception ex){
					ex.printStackTrace();
				}
			}
		});
		button.setToolTipText("Creates an event based on the parameters below");
		button.setBounds(99, 349, 117, 25);
		add(button);
		
		JButton button_1 = new JButton("Edit");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					int sMin = Integer.parseInt(textAreaScoreMin.getText());
					int sMax = Integer.parseInt(textAreaScoreMax.getText());
					int seq = Integer.parseInt(textAreaSortSeq.getText());
					if(event.validateEvent((String)textAreaEventName.getText(), (String)textAreaEventCode.getText(), (String)comboBoxScoreUnit.getSelectedItem(), 
						sMin, sMax,seq) == true){
					
						if(table.getSelectedRow() != -1){
							event.editEvent(table.getSelectedRow(),(String)textAreaEventName.getText(), (String)textAreaEventCode.getText(), (String)comboBoxScoreUnit.getSelectedItem(), 
							sMin, sMax,seq, modelEvents);
						}
					}
					/*else{
						JOptionPane.showMessageDialog(frame,
							    "Error in creating entry");
					}*/
					validate();
					repaint();
				} catch (Exception ex){
					ex.printStackTrace();
				}
			}
		});
		button_1.setToolTipText("Edits the current selected event");
		button_1.setBounds(328, 349, 117, 25);
		add(button_1);
		
		JButton button_2 = new JButton("Delete");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					int sMin = Integer.parseInt(textAreaScoreMin.getText());
					int sMax = Integer.parseInt(textAreaScoreMax.getText());
					int seq = Integer.parseInt(textAreaSortSeq.getText());
					if(event.validateEvent((String)textAreaEventName.getText(), (String)textAreaEventCode.getText(), (String)comboBoxScoreUnit.getSelectedItem(), 
						sMin, sMax,seq) == true){
					
						if(table.getSelectedRow() != -1){
							event.deleteEvent(table.getSelectedRow(), modelEvents);
						}
					}
					/*else{
						JOptionPane.showMessageDialog(frame,
							    "Error in creating entry");
					}*/
					validate();
					repaint();
				} catch (Exception ex){
					ex.printStackTrace();
				}
			}
		});
		button_2.setToolTipText("Deletes the current selected event");
		button_2.setBounds(557, 349, 117, 25);
		add(button_2);
		
		JLabel lblEventName = new JLabel("Event Name");
		lblEventName.setBounds(56, 410, 90, 15);
		add(lblEventName);
		
		JLabel lblEventCode = new JLabel("Event Code");
		lblEventCode.setBounds(56, 446, 90, 15);
		add(lblEventCode);
		
		JComboBox<String> comboBoxScoreUnit = new JComboBox<String>();
		comboBoxScoreUnit.setToolTipText("*Required Field - Select the score unit of this event (None, Time, Distance)");
		comboBoxScoreUnit.setModel(new DefaultComboBoxModel<String>(new String[] {"None", "Distance", "Time"}));
		comboBoxScoreUnit.setBounds(590, 405, 69, 24);
		add(comboBoxScoreUnit);
		
		JLabel lblScoreUnit = new JLabel("Score Unit");
		lblScoreUnit.setBounds(460, 410, 83, 15);
		add(lblScoreUnit);
		
		JLabel lblScoreMinimun = new JLabel("Score Minimun");
		lblScoreMinimun.setBounds(460, 446, 103, 15);
		add(lblScoreMinimun);
		
		JLabel lblScoreMaximun = new JLabel("Score Maximun");
		lblScoreMaximun.setBounds(460, 482, 117, 15);
		add(lblScoreMaximun);
		
		JTextArea textAreaEventName = new JTextArea();
		textAreaEventName.setToolTipText("*Required Field - Enter the name of this event");
		textAreaEventName.setBounds(182, 410, 133, 15);
		add(textAreaEventName);
		
		JTextArea textAreaEventCode = new JTextArea();
		textAreaEventCode.setToolTipText("*Required Field - Enter the event code for this event");
		textAreaEventCode.setBounds(182, 446, 133, 15);
		add(textAreaEventCode);
		
		JTextArea textAreaScoreMin = new JTextArea();
		textAreaScoreMin.setToolTipText("*Required Field - Enter the minimum score to participate in this event");
		textAreaScoreMin.setBounds(590, 446, 69, 15);
		add(textAreaScoreMin);
		
		JTextArea textAreaScoreMax = new JTextArea();
		textAreaScoreMax.setToolTipText("*Required Field - Enter the maximum score to participate in this event");
		textAreaScoreMax.setBounds(590, 482, 69, 15);
		add(textAreaScoreMax);
		
		JLabel lblSortSequence = new JLabel("Sort Sequence");
		lblSortSequence.setBounds(460, 518, 117, 15);
		add(lblSortSequence);
		
		JTextArea textAreaSortSeq = new JTextArea();
		textAreaSortSeq.setToolTipText("*Required Field - Enter the desired sort sequence for this event");
		textAreaSortSeq.setBounds(590, 518, 69, 15);
		add(textAreaSortSeq);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(1, 0, 1, 2));
		contentPane.setLayout(null);
		
	}
}
