package management.ui;

import management.controller.*;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

@SuppressWarnings("serial")
public class HeatsPanel extends JPanel {
	
	private JPanel contentPane;
	private JTable table;
	private GenerateHeatSheetsController g;
	private JComboBox<String> comboBoxEventName = new JComboBox<String>();
	private JComboBox<Integer> comboBoxMinAge = new JComboBox<Integer>();
	private JComboBox<Integer> comboBoxMaxAge = new JComboBox<Integer>();
	private JComboBox<String> comboBoxHour = new JComboBox<String>();
	private JComboBox<String> comboBoxMinute = new JComboBox<String>();
	private JComboBox<String> comboBoxEventCode = new JComboBox<String>();
	private JComboBox<String> comboBoxGender = new JComboBox<String>();
	JComboBox<Integer> comboBoxNumHeats = new JComboBox<Integer>();
	private DefaultTableModel modelHeats = null;
	HeatViewController h = null;
	
	
	public HeatsPanel(GenerateHeatSheetsController ghCtrl, HeatViewController hvc) {
		g = ghCtrl;
		h = hvc;
		setLayout(null);
		setSize(800,590);
		
		JLabel label = new JLabel("Heat Information");
		label.setBounds(342, 12, 120, 15);
		add(label);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(21, 39, 757, 300);
		add(scrollPane);
		
		modelHeats = new DefaultTableModel(new Object[][] {}, new Object[] {"Event Code", "Event Name", "Gender", "Min Age", "Max Age", "Time", "Number of Heats"});
		table = new JTable(modelHeats);
		table.setBorder(new LineBorder(new Color(0, 0, 0)));
		table.setBounds(252, 138, 300, 300);
		scrollPane.setViewportView(table);
		hvc.generateHTable(modelHeats);
		
		JLabel lblEventName = new JLabel("Event Name");
		lblEventName.setBounds(54, 401, 90, 15);
		add(lblEventName);
		
		comboBoxEventName.setToolTipText("*Required Field - Please select event for this heat");
		comboBoxEventName.setBounds(162, 396, 213, 24);
		hvc.generateEventsCBoxName(comboBoxEventName);
		add(comboBoxEventName);
		
		comboBoxMinAge.setToolTipText("*Required Field - Please select the minimun age for this heat");
		comboBoxMinAge.setBounds(162, 446, 62, 24);
		Integer[] age = new Integer[100];
		for (int x = 0; x < 100; x++) {
			age[x] = x;
		}
		comboBoxMinAge.setModel(new DefaultComboBoxModel<Integer>(age));
		add(comboBoxMinAge);
		
		JLabel lblMinAge = new JLabel("Min. Age");
		lblMinAge.setBounds(74, 451, 70, 15);
		add(lblMinAge);
		
		JLabel lblMaxAge = new JLabel("Max Age");
		lblMaxAge.setBounds(236, 451, 70, 15);
		add(lblMaxAge);
		
		comboBoxMaxAge.setToolTipText("*Required Field - Please selected the maximum age for this heat");
		comboBoxMaxAge.setBounds(313, 446, 62, 24);
		comboBoxMaxAge.setModel(new DefaultComboBoxModel<Integer>(age));
		add(comboBoxMaxAge);
		
		JLabel lblTime = new JLabel("Time");
		lblTime.setBounds(92, 501, 52, 15);
		add(lblTime);
		
		comboBoxHour.setToolTipText("*Required Field - Please enter the hour of the time of the heat (0-23)");
		comboBoxHour.setBounds(172, 501, 52, 24);
		comboBoxHour.setModel(new DefaultComboBoxModel<String>(new String[] {"00","01","02","03","04", "05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23"}));
		add(comboBoxHour);
		
		comboBoxMinute.setToolTipText("*Required Field - Please enter the minute time for this heat");
		comboBoxMinute.setBounds(241, 501, 52, 24);
		String[] minutes = new String[60];
		for (int x = 0; x < 60; x++) {
			minutes[x] = x+"";
			if (x < 10)
				minutes[x] = "0"+minutes[x];
		}
		comboBoxMinute.setModel(new DefaultComboBoxModel<String>(minutes));
		add(comboBoxMinute);
		
		JLabel colon = new JLabel(" :");
		colon.setBounds(228, 503, 27, 15);
		add(colon);
		
		JLabel lblGender = new JLabel("Gender");
		lblGender.setBounds(506, 453, 70, 15);
		add(lblGender);
		
		JLabel lblEventCode = new JLabel("Event Code");
		lblEventCode.setBounds(506, 403, 90, 15);
		add(lblEventCode);
		
		comboBoxEventCode.setToolTipText("*Required Field - Please select the corresponding event code for this heat");
		comboBoxEventCode.setBounds(621, 398, 117, 24);
		hvc.generateEventsCBoxCode(comboBoxEventCode);
		add(comboBoxEventCode);
		
		comboBoxGender.setToolTipText("*Required Field - Please enter the gender for this heat (M, F, B)");
		comboBoxGender.setBounds(621, 448, 117, 24);
		comboBoxGender.setModel(new DefaultComboBoxModel<String>(new String[] {" ", "Male", "Female", "Both"}));
		add(comboBoxGender);
		
		JButton btnGenerateHeatSheets = new JButton("Generate Heat Sheets");
		btnGenerateHeatSheets.setToolTipText("Generates a text file  containing the heat sheets for the heats listed above");
		btnGenerateHeatSheets.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				g.generateHeatSheets();
			}
		});
		btnGenerateHeatSheets.setBounds(528, 490, 193, 33);
		add(btnGenerateHeatSheets);
		
		JLabel lblNumberOfHeats = new JLabel("Number of Heats");
		lblNumberOfHeats.setBounds(27, 548, 135, 15);
		add(lblNumberOfHeats);
		
		comboBoxNumHeats.setBounds(162, 543, 62, 24);
		Integer[] num = new Integer[20];
		for (int x = 0; x < num.length; x++) {
			num[x] = x+1;
		}
		comboBoxNumHeats.setModel(new DefaultComboBoxModel<Integer>(num));
		add(comboBoxNumHeats);
		
		JButton btnCreate = new JButton("Create");
		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					int min_age = (Integer)comboBoxMinAge.getSelectedItem();
					int max_age = (Integer)comboBoxMaxAge.getSelectedItem();
					int num_heat = (Integer)comboBoxNumHeats.getSelectedItem();
					if(h.validateHeat((String)comboBoxEventCode.getSelectedItem(), (String)comboBoxEventName.getSelectedItem(), (String)comboBoxGender.getSelectedItem(), 
						min_age, max_age,(String)comboBoxHour.getSelectedItem(),
						(String)comboBoxMinute.getSelectedItem()) == true){
					h.createHeat((String)comboBoxEventCode.getSelectedItem(), (String)comboBoxGender.getSelectedItem(), 
								min_age, max_age,(String)comboBoxHour.getSelectedItem(),
								(String)comboBoxMinute.getSelectedItem(), num_heat, modelHeats);
					}
					modelHeats.fireTableDataChanged();
					validate();
					repaint();
				} catch (Exception ex){
					ex.printStackTrace();
				}
			}
		});
		btnCreate.setToolTipText("Creates heat based on the parameters below");
		btnCreate.setBounds(114, 349, 117, 25);
		add(btnCreate);
		
		JButton btnEdit = new JButton("Edit");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					int min_age = (Integer)comboBoxMinAge.getSelectedItem();
					int max_age = (Integer)comboBoxMaxAge.getSelectedItem();
					int num_heat = (Integer)comboBoxNumHeats.getSelectedItem();
					if(h.validateHeat((String)comboBoxEventCode.getSelectedItem(), (String)comboBoxEventName.getSelectedItem(), (String)comboBoxGender.getSelectedItem(), 
						min_age, max_age,(String)comboBoxHour.getSelectedItem(),
						(String)comboBoxMinute.getSelectedItem()) == true){
						if (table.getSelectedRow() != -1){
							h.editHeat(table.getSelectedRow(),(String)comboBoxEventCode.getSelectedItem(), (String)comboBoxGender.getSelectedItem(), 
									min_age, max_age,(String)comboBoxHour.getSelectedItem(),
									(String)comboBoxMinute.getSelectedItem(), num_heat, modelHeats);
						}
					}
					modelHeats.fireTableDataChanged();
					validate();
					repaint();
			}
		});
		btnEdit.setToolTipText("Edits the current selected heat");
		btnEdit.setBounds(343, 349, 117, 25);
		add(btnEdit);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (table.getSelectedRow() != -1){
					h.deleteHeat(table.getSelectedRow(), modelHeats);
					}
				modelHeats.fireTableDataChanged();
				validate();
				repaint();
			}
		});
		btnDelete.setToolTipText("Deletes the current selected heat");
		btnDelete.setBounds(572, 349, 117, 25);
		add(btnDelete);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(1, 0, 1, 2));
		
		contentPane.setLayout(null);
		
		
	}
}
