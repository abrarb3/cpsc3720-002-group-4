/**
 * This is the ManagementDatabase class that handles connections to the database as well as parsing of CSV files
 * @param None
 * @exception None
 * @return None
 */


package management.db;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.orientechnologies.orient.core.db.ODatabaseRecordThreadLocal;
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.id.ORecordId;
import com.orientechnologies.orient.core.record.impl.ODocument;

import management.model.*;

@SuppressWarnings("serial")
public class ManagementDatabase implements Serializable{
	
	private static ManagementDatabase instance;
	private ODatabaseDocumentTx db;
	private ArrayList<School> schoolList = new ArrayList<School>();						//list of schools
	private ArrayList<GroupLeader> groupLeaderList = new ArrayList<GroupLeader>();		//list of group leaders
	private ArrayList<Athletes> athleteList = new ArrayList<Athletes>();				//list of athletes
	private ArrayList<Events> eventList = new ArrayList<Events>();						//list of events
	private ArrayList<Heats> heatList = new ArrayList<Heats>();							//list of heats

	private ArrayList<String[]> athletes = parseFile("athletes.csv");	//used to retain data from the athlete file
	private ArrayList<String[]> events = parseFile("events.csv");		//used to retain data from the event file
	private ArrayList<String[]> heats = parseFile("heats.csv");		//used to retain data from the heat file
	
	/**
	 * This is the initial setup the program goes through to load all of the data from the database or CSV file
	 * @param None
	 * @exception None
	 * @return None
	 */

	private ManagementDatabase(){
		
		db = new ODatabaseDocumentTx(
		        "remote:localhost:2424/DomainModel");
		db.open("root", "iamgroot");
		
		/* Create group leaders and athletes */
		for (int x = 1; x < athletes.size(); x++) {
			boolean schoolListContains = false;
			boolean groupLeaderListContains = false;
			boolean athleteExists = false;
			for (int y = 0; y < schoolList.size(); y++) {
				if (schoolList.get(y).getSchoolname().equals(athletes.get(x)[1])) {
					schoolListContains = true;
				}
			}
			for (int y = 0; y < groupLeaderList.size(); y++){// checks if group leader has already been added
				if (groupLeaderList.get(y).getGroupLeaderName().equals(athletes.get(x)[2])) {
					groupLeaderListContains = true;
				}
			}
			for (int y = 0; y < athleteList.size(); y++) {
				if (athleteList.get(y).getFirstName().equals(athletes.get(x)[3]) && athleteList.get(y).getLastName().equals(athletes.get(x)[4])) {
					athleteList.get(y).setEventcode2(athletes.get(x)[7]);
					athleteList.get(y).setScore2(Integer.parseInt(athletes.get(x)[9]));
					athleteExists = true;
				}
			}
			
			GroupLeader newLeader = null;
			if (!athleteExists) {
				if (!schoolListContains) {
					School newSchool = new School(athletes.get(x)[0], athletes.get(x)[1]);
					if (!groupLeaderListContains) {// makes a new group leader if not added
						newLeader = new GroupLeader(athletes.get(x)[0], newSchool, athletes.get(x)[2]);
						Athletes newAthlete = new Athletes(athletes.get(x)[3], athletes.get(x)[4], Integer.parseInt(athletes.get(x)[5]), athletes.get(x)[6], athletes.get(x)[7], athletes.get(x)[0], Integer.parseInt(athletes.get(x)[9]), newLeader);
						//newLeader.getListAthletes().add(newAthlete);
						groupLeaderList.add(newLeader);
						athleteList.add(newAthlete);
					}
					else {// adds the athlete to the list of athletes for the group leader
						Athletes newAthlete = new Athletes(athletes.get(x)[3], athletes.get(x)[4], Integer.parseInt(athletes.get(x)[5]), athletes.get(x)[6], athletes.get(x)[7], athletes.get(x)[0], Integer.parseInt(athletes.get(x)[9]), newLeader);
						for (int y = 0; y < groupLeaderList.size(); y++) {
							if (groupLeaderList.get(y).getGroupLeaderName().equals(athletes.get(x)[2])) {
								//groupLeaderList.get(y).getListAthletes().add(newAthlete);
								athleteList.add(newAthlete);
							}
						}
					}
					newSchool.getListGroupLeaders().add(newLeader);
					schoolList.add(newSchool);
				}
				else {
					for (int y = 0; y < schoolList.size(); y++) {
						if (schoolList.get(y).getSchoolname().equals(athletes.get(x)[1])) {
							schoolList.get(y).getListGroupLeaders().add(newLeader);
							if (!groupLeaderListContains) {// makes a new group leader if not added
								newLeader = new GroupLeader(athletes.get(x)[0], schoolList.get(y), athletes.get(x)[2]);
								Athletes newAthlete = new Athletes(athletes.get(x)[3], athletes.get(x)[4], Integer.parseInt(athletes.get(x)[5]), athletes.get(x)[6], athletes.get(x)[7], athletes.get(x)[0], Integer.parseInt(athletes.get(x)[9]), newLeader);
								//newLeader.getListAthletes().add(newAthlete);
								groupLeaderList.add(newLeader);
								athleteList.add(newAthlete);
							}
							else {// adds the athlete to the list of athletes for the group leader
								Athletes newAthlete = new Athletes(athletes.get(x)[3], athletes.get(x)[4], Integer.parseInt(athletes.get(x)[5]), athletes.get(x)[6], athletes.get(x)[7], athletes.get(x)[0], Integer.parseInt(athletes.get(x)[9]), newLeader);
								for (int z = 0; z < groupLeaderList.size(); z++) {
									if (groupLeaderList.get(z).getGroupLeaderName().equals(athletes.get(x)[2])) {
										//groupLeaderList.get(z).getListAthletes().add(newAthlete);
										athleteList.add(newAthlete);
									}
								}
							}
						}
					}
				}
			}
		}
		
		/* Create events */
		for (int x = 1; x < events.size(); x++) {
			Events newEvents = new Events(events.get(x)[1], events.get(x)[0], events.get(x)[2], Integer.parseInt(events.get(x)[3]), Integer.parseInt(events.get(x)[4]), Integer.parseInt(events.get(x)[5]));
			/* Create heats */
			for (int y = 1; y < heats.size(); y++){
				boolean heatExists = false;
				Heats newHeats = new Heats(heats.get(y)[0], heats.get(y)[2], Integer.parseInt(heats.get(y)[3]), Integer.parseInt(heats.get(y)[4]), heats.get(y)[5], Integer.parseInt(heats.get(y)[6]), newEvents);
				if (newHeats.getEventCode().equals(newEvents.getEventCode())) {
					newEvents.getListHeats().add(newHeats);
				}
				for (int i = 0; i < heatList.size(); i++) {
					if (heatList.get(i).compareHeat(newHeats)) {
						heatExists = true;
					}
				}
				if (!heatExists) {
					heatList.add(newHeats);
				}
			}
			eventList.add(newEvents);
		}	
		
		if (retrieveEvents().size() == 0 || retrieveHeats().size() == 0 || retrieveSchool().size() == 0 || retrieveGroupLeader().size() == 0 || retrieveAthletes().size() == 0) {
			//Send Group Leader data to the database
			for (int x = 0; x < groupLeaderList.size(); x++) {
				createGroupLeader(db, groupLeaderList.get(x));
			}
			//Send Athlete data to the database
			for (int x = 0; x < athleteList.size(); x++) {
				createAthlete(athleteList.get(x));
			}
			//Send Event data to the database
			for (int x = 0; x < eventList.size(); x++) {
				createEvent(eventList.get(x));
			}
			//Send Heat data to the database
			for (int x = 0; x < heatList.size(); x++) {
				createHeat(heatList.get(x));
			}
			//Send School data to the database
			for (int x = 0; x < schoolList.size(); x++) {
				createSchool(db, schoolList.get(x));
			}
		}
	}

	/* Creates a singleton for the database */
	public static ManagementDatabase instance() {
		if (instance == null) {
			instance  = new ManagementDatabase();
		}
		return instance;
	}
	
	/* used to parse CSV files */
	public ArrayList<String[]> parseFile(String fileName) {
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		ArrayList<String[]> aList = new ArrayList<String[]>();
		
		try {
			br = new BufferedReader(new FileReader(fileName));
			while ((line = br.readLine()) != null) {
				aList.add(line.split(cvsSplitBy));
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return aList;
	}
	
	/* used to send School data to the database*/
	private static void createSchool(ODatabaseDocumentTx db, School school) {
		ODocument doc = new ODocument("s");
	    doc.fromJSON(new Gson().toJson(school));
	    doc.save();
	    
	    String id = doc.getIdentity().toString();
	    school.setId(id);
	}
	
	/* used to send Group Leader data to the database */
	private static void createGroupLeader(ODatabaseDocumentTx db, GroupLeader gl) {
	    ODocument doc = new ODocument("g");
	    doc.fromJSON(new Gson().toJson(gl));
	    doc.save();
	    
	    String id = doc.getIdentity().toString();
	    gl.setId(id);
	}
	/* used to send Athlete data to the database */
	public void createAthlete(Athletes a) {
		ODatabaseRecordThreadLocal.INSTANCE.set(db);
	    ODocument doc = new ODocument("a");
	    doc.fromJSON(new Gson().toJson(a));
	    doc.save();
	    
	    String id = doc.getIdentity().toString();
	    a.setId(id);
	}
	/* used to send Event data to the database */
	public void createEvent(Events e) {
		ODatabaseRecordThreadLocal.INSTANCE.set(db);
	    ODocument doc = new ODocument("e");
	    doc.fromJSON(new Gson().toJson(e));
	    doc.save();
	    
	    String id = doc.getIdentity().toString();
	    e.setId(id);
	}
	
	/* used to send Heat data to the database */
	public void createHeat(Heats h) {
		ODatabaseRecordThreadLocal.INSTANCE.set(db);
	    ODocument doc = new ODocument("h");
	    doc.fromJSON(new Gson().toJson(h));
	    doc.save();
	    String id = doc.getIdentity().toString();
	    h.setId(id);
	}

	/* used to edit athletes */
	public void updateAthlete(Athletes a) {
		ODatabaseRecordThreadLocal.INSTANCE.set(db);
		ODocument doc = db.getRecord(new ORecordId(a.getId()));
		doc.fromJSON(new Gson().toJson(a));
		doc.save();
	}
	
	/* used to edit events */
	public void updateEvent(Events e) {
		ODatabaseRecordThreadLocal.INSTANCE.set(db);
		ODocument doc = db.getRecord(new ORecordId(e.getId()));
		doc.fromJSON(new Gson().toJson(e));
		doc.save();
	}
	
	/* used to edit heats */
	public void updateHeat(Heats h) {
		ODatabaseRecordThreadLocal.INSTANCE.set(db);
		ODocument doc = db.getRecord(new ORecordId(h.getId()));
		doc.fromJSON(new Gson().toJson(h));
		doc.save();
	}
	
	/* used to delete athletes */
	public void deleteAthlete(Athletes a) {
		ODatabaseRecordThreadLocal.INSTANCE.set(db);
		ODocument doc = db.getRecord(new ORecordId(a.getId()));
		doc.fromJSON(new Gson().toJson(a));
		doc.delete();
	}
	
	/* used to delete events */
	public void deleteEvent(Events e) {
		ODatabaseRecordThreadLocal.INSTANCE.set(db);
		ODocument doc = db.getRecord(new ORecordId(e.getId()));
		doc.fromJSON(new Gson().toJson(e));
		doc.delete();
	}
	
	/* used to delete heats */
	public void deleteHeat(Heats h) {
		ODatabaseRecordThreadLocal.INSTANCE.set(db);
		ODocument doc = db.getRecord(new ORecordId(h.getId()));
		doc.fromJSON(new Gson().toJson(h));
		doc.delete();
	}
	
	
	ArrayList<School> sList = new ArrayList<School>();
	ArrayList<GroupLeader> gList = new ArrayList<GroupLeader>();
	
	/* used to retrieve events from the database */
	public ArrayList<Events> retrieveEvents() {
		ArrayList<Events> eList = new ArrayList<Events>();
		for (ODocument Events : db.browseClass("Events")) {
			Events newEvent = new Gson().fromJson(Events.toJSON(), Events.class);
			newEvent.setEventCode(Events.field("EventCode").toString());
			newEvent.setEventName(Events.field("EventName").toString());
			newEvent.setScoremax((Integer)Events.field("scoremax"));
			newEvent.setScoremin((Integer)Events.field("scoremin"));
			newEvent.setScoreunit(Events.field("scoreunit").toString());
			newEvent.setSortseq((Integer)Events.field("sortseq"));
			newEvent.setId(Events.getIdentity().toString());
			eList.add(newEvent);
		}
		return eList;
	}
	
	/* used to retrieve heats from the database */
	public ArrayList<Heats> retrieveHeats() {
		ArrayList<Events> eList = new ArrayList<Events>();
		eList = retrieveEvents();
		ArrayList<Heats> hList = new ArrayList<Heats>();
		for (ODocument Heats : db.browseClass("Heats")) {
			Heats newHeat = new Gson().fromJson(Heats.toJSON(), Heats.class);
			newHeat.setEventCode(Heats.field("EventCode").toString());
			newHeat.setGender(Heats.field("gender").toString());
			newHeat.setMaxAge((Integer)(Heats.field("maxage")));
			newHeat.setMinAge((Integer)(Heats.field("minage")));
			newHeat.setNumHeats((Integer)(Heats.field("numheats")));
			newHeat.setTime(Heats.field("time").toString());
			for (int x = 0; x < eList.size(); x++) {
				if (eList.get(x).getEventCode().equals(newHeat.getEventCode())) {
					newHeat.setEventName(eList.get(x));
					//eList.get(x).getListHeats().add(newHeat);
				}
			}
			newHeat.setId(Heats.getIdentity().toString());
			hList.add(newHeat);
		}
		return hList;
	}
	
	/* used to retrieve schools from the database */
	public ArrayList<School> retrieveSchool() {
		for (ODocument School: db.browseClass("School")) {
			School newSchool = new Gson().fromJson(School.toJSON(), School.class);
			newSchool.setGroupcode(School.field("groupcode").toString());
			newSchool.setSchoolname(School.field("schoolname").toString());
			newSchool.setId(School.getIdentity().toString());
			sList.add(newSchool);
		}
		return sList;
	}
	
	/* used to retrieve group leaders from the database */
	public ArrayList<GroupLeader> retrieveGroupLeader() {
		for (ODocument GroupLeader : db.browseClass("GroupLeader")) {
			GroupLeader newGroupLeader = new Gson().fromJson(GroupLeader.toJSON(), GroupLeader.class);
			newGroupLeader.setGroupCode(GroupLeader.field("groupcode").toString());
			newGroupLeader.setGroupLeaderName(GroupLeader.field("GroupLeaderName").toString());
			for (int x = 0; x < sList.size(); x++) {
				if (sList.get(x).getGroupcode().equals(newGroupLeader.getGroupCode())) {
					newGroupLeader.setSchoolName(sList.get(x));
					//sList.get(x).getListGroupLeaders().add(newGroupLeader);
				}
			}
			newGroupLeader.setId(GroupLeader.getIdentity().toString());
			gList.add(newGroupLeader);
		}
		return gList;
	}	
	
	/* used to retrieve athletes from the database */
	public ArrayList<Athletes> retrieveAthletes() {	
		ArrayList<Athletes> aList = new ArrayList<Athletes>();
		for (ODocument Athletes : db.browseClass("Athlete")) {
			Athletes newAthlete = new Gson().fromJson(Athletes.toJSON(), Athletes.class);
			newAthlete.setAge((Integer)Athletes.field("age"));
			newAthlete.setGender(Athletes.field("gender").toString());
			newAthlete.setEventcode1(Athletes.field("eventcode1").toString());
			if (Athletes.field("eventcode2") != null) {
				newAthlete.setEventcode2(Athletes.field("eventcode2").toString());
			}
			newAthlete.setFirstName(Athletes.field("FirstName").toString());
			newAthlete.setLastName(Athletes.field("LastName").toString());
			newAthlete.setGroupCode(Athletes.field("groupCode").toString());
			newAthlete.setScore1(Integer.parseInt(Athletes.field("score1").toString()));
			if (Athletes.field("score2") != null)  {
				newAthlete.setScore2(Integer.parseInt(Athletes.field("score2").toString()));
			}
			for (int x = 0; x < gList.size(); x++) {
				if (newAthlete.getGroupCode().equals(gList.get(x).getGroupCode())) {
					newAthlete.setGroupLeader(gList.get(x));
					//gList.get(x).getListAthletes().add(newAthlete);
				}
			}
			newAthlete.setId(Athletes.getIdentity().toString());
			aList.add(newAthlete);
		}
		
		return aList;
	}
	
	/*returns the database doc */
	public ODatabaseDocumentTx getDatabase() {
		return  db;
	}
}
