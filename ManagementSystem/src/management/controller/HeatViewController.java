package management.controller;

import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.table.DefaultTableModel;

import management.db.ManagementDatabase;
import management.model.*;

public class HeatViewController {

	ManagementDatabase datab = null;
	//ArrayList<Heats> hList = new ArrayList<Heats>();
	ArrayList<Events> eList = new ArrayList<Events>();
	
	/**
	 * This is the constructor for the controller
	 * @param ManagementDatabase d - the database class that has access to the OrientDB database
	 * @exception None
	 * @return a new Instance of the controller
	 */
	public HeatViewController() {
		datab = ManagementDatabase.instance();
		eList = datab.retrieveEvents();
		datab.retrieveSchool();
		datab.retrieveGroupLeader();
		datab.retrieveAthletes();
	}
	
	public void createHeat(String code, String gen, int min, int max, String h, String m, int numHeats, DefaultTableModel modelHeats){
		String time = h.concat(":").concat(m);
		Heats create = null;
		Events event = null;
		for (int x = 0; x < eList.size(); x++) {
			if (eList.get(x).getEventCode().equals(code)) {
				event = eList.get(x);
			}
		}
		create = new Heats(code, gen, min, max, time, numHeats, event);
		String eCode = create.getEventCode();
		String eName = create.getEventName().getEventName();
		String gender = create.getGender();
		int minAge = create.getMinAge();
		int maxAge = create.getMaxAge();
		String ntime = create.getTime();
		int nHeats = create.getNumHeats();
		Object aData [] = {eCode, eName, gender, minAge, maxAge, ntime, nHeats};
		modelHeats.addRow(aData);
		datab.createHeat(create);	
	}
	
	public void deleteHeat(int e, DefaultTableModel modelHeats){
		ArrayList <Heats>hList = datab.retrieveHeats();
		Heats del = hList.get(e);
		modelHeats.removeRow(e);
		datab.deleteHeat(del);
	}
	
	public void editHeat(int e, String eCode, String gender, int minAge, int maxAge,String hour, String minute, int numHeats, DefaultTableModel modelHeats){
		ArrayList <Heats>hList = datab.retrieveHeats();
		Heats newHeat = hList.get(e);
		newHeat.setEventCode(eCode);
		newHeat.setGender(gender);
		newHeat.setMinAge(minAge);
		newHeat.setMaxAge(maxAge);
		newHeat.setTime(hour+":"+minute);
		newHeat.setNumHeats(numHeats);
		for (int x = 0; x < eList.size(); x++) {
			if (eList.get(x).getEventCode().equals(eCode)) {
				newHeat.setEventName(eList.get(x));
			}
		}
		modelHeats.setValueAt(newHeat.getEventCode(), e, 0);
		modelHeats.setValueAt(newHeat.getEventName().getEventName(), e, 1);
		modelHeats.setValueAt(newHeat.getGender(), e, 2);
		modelHeats.setValueAt(newHeat.getMinAge(), e, 3);
		modelHeats.setValueAt(newHeat.getMaxAge(), e, 4);
		modelHeats.setValueAt(newHeat.getTime(), e, 5);
		modelHeats.setValueAt(newHeat.getNumHeats(), e, 6);
		datab.updateHeat(newHeat);
	}
	
	public Boolean validateHeat(String code, String name, String gen, int min, int max, String h, String m){
		if(code!= null){
			if(name!=null){
				if(gen!=null){
					if(Integer.toString(min)!=null){
						if(Integer.toString(max)!=null){
							if(h!=null){
								if(m!=null){
									return true;
								}
							}
						}
					}
				}
			}
			
		}
		return false;
	}
	
	public void generateHTable(DefaultTableModel modelHeats) {
		ArrayList<Heats> hList = datab.retrieveHeats();
		for (int x = 0; x < hList.size(); x++) {
			String eCode = hList.get(x).getEventCode();
			String eName = hList.get(x).getEventName().getEventName();
			String gender = hList.get(x).getGender();
			int minAge = hList.get(x).getMinAge();
			int maxAge = hList.get(x).getMaxAge();
			String time = hList.get(x).getTime();
			int nHeats = hList.get(x).getNumHeats();
			Object aData [] = {eCode, eName, gender, minAge, maxAge, time, nHeats};
			modelHeats.addRow(aData);
		}
	}
	
	public void generateEventsCBoxCode(JComboBox<String> comboBoxEventCode) {
		String[] listEvents= new String[eList.size()];
		for (int x = 0; x < eList.size(); x++) {
			listEvents[x] = eList.get(x).getEventCode();
		}
		comboBoxEventCode.setModel(new DefaultComboBoxModel<String>(listEvents));
	}
	
	public void generateEventsCBoxName(JComboBox<String> comboBoxEventName) {
		String[] listEvents= new String[eList.size()];
		for (int x = 0; x < eList.size(); x++) {
			listEvents[x] = eList.get(x).getEventName();
		}
		comboBoxEventName.setModel(new DefaultComboBoxModel<String>(listEvents));
	}
	
}
