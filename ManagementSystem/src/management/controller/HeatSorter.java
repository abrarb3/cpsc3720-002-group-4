package management.controller;

import java.util.Comparator;

import management.model.Heats;

public class HeatSorter implements Comparator<Heats> {

	public int compare(Heats one, Heats two) {
		return one.compareTo(two);
	}
}
