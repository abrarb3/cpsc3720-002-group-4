/**
 * This handles the Generate Heat Sheets use case.
 * This is used when the user selects the Generate Heat Sheets button in the GUI
 * @param None
 * @exception None
 * @return None
 */

package management.controller;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import management.db.*;
import management.model.*;

public class GenerateHeatSheetsController {

	private ArrayList<Heats> hList = new ArrayList<Heats>();	
	private ArrayList<Athletes> aList = new ArrayList<Athletes>();
	private ManagementDatabase db;
	
	/**
	 * This is the constructor for the controller
	 * @param None
	 * @exception None
	 * @return a new Instance of the controller
	 */
	public GenerateHeatSheetsController() {
		db = ManagementDatabase.instance();
	}
	
	/**
	 * What the button actually calls to generate the heat sheets.
	 * It just prints the information to a text file "heats.txt"
	 * @param None
	 * @exception None
	 * @return None
	 */
	public void generateHeatSheets() {
		System.out.println("Generating heat sheets...");
		
		int pageNumber = 1;
		int lineTracker = 5;
		int division = 1;
		boolean newPage = false;
		
		db.retrieveEvents();
		hList = db.retrieveHeats();
		db.retrieveSchool();
		db.retrieveGroupLeader();
		aList = db.retrieveAthletes();
		//58 lines
		FileWriter outFile = null;
		Collections.sort(hList, new HeatSorter());
		try {
			outFile = new FileWriter("heat.txt");
			PrintWriter out = new PrintWriter(outFile);
			for (int i = 0; i < hList.size(); i++) {
				ArrayList<Athletes> athletesInHeat = new ArrayList<Athletes>();//keeps track of the athletes in the heat
				boolean alreadyIn = false;
				
				for (int x = 0; x < aList.size(); x++) {// cycles through the athletes to check if they are in the heat
					for (int y = 0; y < athletesInHeat.size(); y++) {
						if (athletesInHeat.get(y).getFirstName().equals(aList.get(x).getFirstName()) && athletesInHeat.get(y).getLastName().equals((aList.get(x).getLastName()))) {
							alreadyIn = true;
						}
					}
					if (isInHeat(aList.get(x), hList.get(i)) && !alreadyIn) {
						athletesInHeat.add(aList.get(x));
					}
				}
				if (athletesInHeat.size() != 0) {//makes sure the heat has athletes to compete in it
					Collections.sort(athletesInHeat, new NameSorter());
					/* This is the header for each page*/
					out.println(String.format("%-22s %1s %1s to %-5s %s %-27s %1s %1s", hList.get(i).getEventName().getEventName(), "Group", hList.get(i).getMinAge(), hList.get(i).getMaxAge(), "Time", hList.get(i).getTime(), "Page", pageNumber));
					String gender = "Both";
					String scoreUnit = "     ";
					if (hList.get(i).getEventName().getScoreunit().equals("T")) {
						scoreUnit = "Time";
					}
					else if (hList.get(i).getEventName().getScoreunit().equals("D")) {
						scoreUnit = "Distance";
					}
					if (hList.get(i).getGender().equals("M"))
						gender = "Men";
					else if (hList.get(i).getGender().equals("F"))
						gender = "Women";
					
					out.println("For "+ gender);
					out.println("-----------------------------------------------------------------------------------");
					out.println(String.format("%-25s %1s %1s %1s %1s %1s %-11s %18s" , "Name", "Age", "Sx", "Rank", scoreUnit, "Div.", "Group", "Supervisor"));
					out.println("-----------------------------------------------------------------------------------");
					for (int x = 0; x < athletesInHeat.size(); x++) {
						if (newPage) {
							/* The header for each page*/
							out.println(String.format("%-22s %1s %1s to %-5s %s %-27s %1s %1s", hList.get(i).getEventName().getEventName(), "Group", hList.get(i).getMinAge(), hList.get(i).getMaxAge(), "Time", hList.get(i).getTime(), "Page", pageNumber));
							gender = "Both";
							scoreUnit = "     ";
							if (hList.get(i).getEventName().getScoreunit().equals("T")) {
								scoreUnit = "Time";
							}
							else if (hList.get(i).getEventName().getScoreunit().equals("D")) {
								scoreUnit = "Distance";
							}
							if (hList.get(i).getGender().equals("M"))
								gender = "Men";
							else if (hList.get(i).getGender().equals("F"))
								gender = "Women";
							
							out.println("For "+ gender);
							out.println("-----------------------------------------------------------------------------------");
							out.println(String.format("%-25s %1s %1s %1s %1s %1s %-11s %18s" , "Name", "Age", "Sx", "Rank", scoreUnit, "Div.", "Group", "Supervisor"));
							out.println("-----------------------------------------------------------------------------------");
							newPage = false;
							lineTracker = 5;
						}
						out.print(athletesInHeat.get(x).toString1());
						out.print(String.format(" %2s %6s %3s", x+1, athletesInHeat.get(x).printScore(hList.get(i).getEventName().getScoreunit(), hList.get(i).getEventCode()), division));
						out.println(athletesInHeat.get(x).toString2());
						lineTracker++;
						if (lineTracker > 57) {// only 58 lines per page
							newPage = true;
							out.print((char)12);
							lineTracker = 5;
							pageNumber++;
						}
						if ((lineTracker-5) > athletesInHeat.size()/hList.get(i).getNumHeats()) {// checks if there are multiple divisions
							newPage = true;
							out.print((char)12);
							lineTracker = 5;
							pageNumber++;
							division++;
						}
					}
					out.print((char)12);
					division = 1;
					pageNumber++;
					lineTracker = 5;
				}
			}
			out.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		System.out.println("Done generating heat sheets!");
	}
	
	/**
	 * Used to check if the athlete is in the heat that is currently being printed to file
	 * @param Athletes a - athlete to be considered to add
	 * @param Heats h - current sheet being printed out
	 * @exception None
	 * @return boolean - True/False is the Athlete in this Heat?
	 */
	public boolean isInHeat(Athletes a, Heats h) {
		if (a.getAge() >= h.getMinAge() && a.getAge() <= h.getMaxAge()) {
			if (a.getEventcode2() != null) {
				if ((a.getEventcode1().equals(h.getEventCode()) || a.getEventcode2().equals(h.getEventCode()))) {
					if (h.getGender().equals("B")) {
						return true;
					}
					else if (h.getGender().equals(a.getGender())) {
						return true;
					}
				}
			}
			else {
				if ((a.getEventcode1().equals(h.getEventCode()))) {
					if (h.getGender().equals("B")) {
						return true;
					}
					else if (h.getGender().equals(a.getGender())) {
						return true;
					}
				}
			}
			
		}
		return false;
	}
	
}
