package management.controller;

import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;
import management.db.ManagementDatabase;
import management.model.School;

import com.orientechnologies.orient.core.db.document.ODatabaseDocument;

public class SchoolViewController {
	ManagementDatabase datab = null;
	ODatabaseDocument doc = null;
	ArrayList<School> sList = new ArrayList<School>();
	
	/**
	 * This is the constructor for the controller
	 * @param ManagementDatabase d - the database class that has access to the OrientDB database
	 * @exception None
	 * @return a new Instance of the controller
	 */
	public SchoolViewController() {
		datab = ManagementDatabase.instance();
		datab.retrieveEvents();
		doc = datab.getDatabase();
		datab.retrieveHeats();
		sList = datab.retrieveSchool();
		datab.retrieveGroupLeader();
		datab.retrieveAthletes();
	}
	
	public void generateGTable(DefaultTableModel modelGroup) {
		for (int x = 0; x < sList.size(); x++) {
			String groupCode = sList.get(x).getGroupcode();
			String schoolName = sList.get(x).getSchoolname();
			Object aData [] = {groupCode, schoolName};
			modelGroup.addRow(aData);
		}
	}
}
