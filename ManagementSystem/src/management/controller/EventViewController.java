/**
 * This handles the Event management use cases.
 * @param None
 * @exception None
 * @return None
 */

package management.controller;

import java.util.ArrayList;

import javax.swing.table.DefaultTableModel;

import com.orientechnologies.orient.core.db.document.ODatabaseDocument;

import management.db.ManagementDatabase;
import management.model.*;

public class EventViewController {

	ManagementDatabase datab = null;
	ODatabaseDocument doc = null;
	ArrayList<Events> eList = new ArrayList<Events>();
	
	/**
	 * This is the constructor for the controller
	 * @param ManagementDatabase d - the database class that has access to the OrientDB database
	 * @exception None
	 * @return a new Instance of the controller
	 */
	public EventViewController () {
		datab = ManagementDatabase.instance();
		doc = datab.getDatabase();
		eList = datab.retrieveEvents();
		datab.retrieveHeats();
		datab.retrieveSchool();
		datab.retrieveGroupLeader();
		datab.retrieveAthletes();
	}
	
	/**
	 * This is the method that creates a new event through the controller.
	 * @param String name - name of the event
	 * @param String code - code of the event
	 * @param String sUnit - the type of unit for scores
	 * @param int sMin - the  minimum score for an event
	 * @param int sMax - the maximum score for an event
	 * @param int sSeq - the sorting sequence for events
	 * @param DefaultTableModel modelEvents - The table that represents all of the events from database
	 * @exception None
	 * @return None
	 */
	public void createEvent(String eName, String eCode, String sUnit, int sMin, int sMax, int sSeq, DefaultTableModel modelEvents){
		Events create = null;
		//Events event = null;
		/*for (int x = 0; x < eList.size(); x++) {
			if (eList.get(x).getEventCode().equals(eCode)) {
				event = eList.get(x);
			}
		}*/
		create = new Events(eName, eCode, sUnit, sMin, sMax, sSeq);
		datab.createEvent(create);	
		Object eData[] = {eName, eCode, sUnit, sMin, sMax, sSeq};
		modelEvents.addRow(eData);
	}
	
	/**
	 * This is the method for deleting an event.
	 * @param int e - is the row selected by user for deletion
	 * @param DefaultTableModel modelEvents - the table that models all events
	 * @exception None
	 * @return None
	 */
	public void deleteEvent(int e,DefaultTableModel modelEvents){
		Events del = eList.get(e);
		modelEvents.removeRow(e);
		datab.deleteEvent(del);
	}
	
	/**
	 * This is the method that edits an event.
	 * @param String name - name of the event
	 * @param String code - code of the event
	 * @param String sUnit - the type of unit for scores
	 * @param int sMin - the  minimum score for an event
	 * @param int sMax - the maximum score for an event
	 * @param int sSeq - the sorting sequence for events
	 * @param DefaultTableModel modelEvents - The table that represents all of the events from database
	 * @exception None
	 * @return None
	 */
	public void editEvent(int e, String eName, String eCode, String sUnit, int sMin, int sMax, int sSeq, DefaultTableModel modelEvents){
		Events edit = eList.get(e);
		edit.setEventCode(eCode);
		edit.setEventName(eName);
		edit.setScoreunit(sUnit);
		edit.setScoremin(sMin);
		edit.setScoremax(sMax);
		edit.setSortseq(sSeq);
		
		modelEvents.setValueAt(edit.getEventCode(), e, 0);
		modelEvents.setValueAt(edit.getEventName(), e, 1);
		modelEvents.setValueAt(edit.getScoreunit(), e, 2);
		modelEvents.setValueAt(edit.getScoremin(), e, 3);
		modelEvents.setValueAt(edit.getScoremax(), e, 4);
		modelEvents.setValueAt(edit.getSortseq(), e, 5);
		datab.updateEvent(edit);
	}
	
	/**
	 * This is the method that checks to make sure of no blank components.
	 * @param String name - name of the event
	 * @param String code - code of the event
	 * @param String sUnit - the type of unit for scores
	 * @param int sMin - the  minimum score for an event
	 * @param int sMax - the maximum score for an event
	 * @param int sSeq - the sorting sequence for events
	 * @exception None
	 * @return Boolean of either true or false for validation
	 */
	public Boolean validateEvent( String eName, String eCode, String sUnit, int sMin, int sMax, int sSeq){
		if(eCode!= null){
			if(eName!=null){
				if(sUnit!=null){
					if(sMin<0){
						if(sMax<0){
							if(sSeq<0){
								
									return true;
								
							}
						}
					}
				}
			}
			
		}
		return false;
	}
	
	/**
	 * This is the method that makes an event table with all of its components.
	 * @param DefaultTableModel modelEvents - The table that represents all of the events from database
	 * @exception None
	 * @return None
	 */
	public void generateETable(DefaultTableModel modelEvents) {
		for (int x = 0; x < eList.size(); x++) {
			String eCode = eList.get(x).getEventCode();
			String eName = eList.get(x).getEventName();
			String sUnit = eList.get(x).getScoreunit();
			int sMin = eList.get(x).getScoremin();
			int sMax = eList.get(x).getScoremax();
			int sSeq = eList.get(x).getSortseq();
			Object aData [] = {eCode, eName, sUnit, sMin, sMax, sSeq};
			modelEvents.addRow(aData);
		}
	}
	
}
