package management.controller;

import java.util.ArrayList;

import javax.swing.table.DefaultTableModel;

import management.db.ManagementDatabase;
import management.model.GroupLeader;

import com.orientechnologies.orient.core.db.document.ODatabaseDocument;

public class GroupLeaderViewController {
	ManagementDatabase datab = null;
	ODatabaseDocument doc = null;
	ArrayList<GroupLeader> gList = new ArrayList<GroupLeader>();
	
	/**
	 * This is the constructor for the controller
	 * @param ManagementDatabase d - the database class that has access to the OrientDB database
	 * @exception None
	 * @return a new Instance of the controller
	 */
	public GroupLeaderViewController() {
		datab = ManagementDatabase.instance();
		datab.retrieveEvents();
		doc = datab.getDatabase();
		datab.retrieveHeats();
		datab.retrieveSchool();
		gList = datab.retrieveGroupLeader();
		datab.retrieveAthletes();
	}
	
	public void generateGTable(DefaultTableModel modelGroup) {
		for (int x = 0; x < gList.size(); x++) {
			String groupCode = gList.get(x).getGroupCode();
			String groupLeaderName = gList.get(x).getGroupLeaderName();
			Object aData [] = {groupCode, groupLeaderName};
			modelGroup.addRow(aData);
		}
	}
}
