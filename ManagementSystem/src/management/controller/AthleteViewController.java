/**
 * This handles the Athlete management use cases.
 * @param None
 * @exception None
 * @return None
 */

package management.controller;

import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.table.DefaultTableModel;
import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import management.db.ManagementDatabase;
import management.model.*;

public class AthleteViewController {

	ManagementDatabase datab = null;
	ODatabaseDocument doc = null;
	ArrayList<Athletes> aList = new ArrayList<Athletes>();
	ArrayList<Events> eList = new ArrayList<Events>();
	
	/**
	 * This is the constructor for the controller
	 * @param ManagementDatabase d - the database class that has access to the OrientDB database
	 * @exception None
	 * @return a new Instance of the controller
	 */
	public AthleteViewController() {
		datab = ManagementDatabase.instance();
		eList = datab.retrieveEvents();
		doc = datab.getDatabase();
		datab.retrieveHeats();
		datab.retrieveSchool();
		datab.retrieveGroupLeader();
		aList = datab.retrieveAthletes();
	}

	public void createAthlete(String first, String last, int age, String gender, String eCode1, String eCode2, String gCode, int aScore1, int aScore2, GroupLeader gl){
		Athletes create = null;
		create = new Athletes(first, last, age, gender, eCode1, gCode, aScore1, gl);
		if (eCode2 != null) {
			create.setEventcode2(eCode2);
			create.setScore2(aScore2);
		}
		datab.createAthlete(create);
	}
	
	public void deleteAthlete(int a, String first, String last, int age, String gender, String eCode1, String gCode, int aScore, GroupLeader gl){
		Athletes del = aList.get(a);
		del.setFirstName(first);
		del.setLastName(last);
		del.setAge(age);
		del.setGender(gender);
		del.setEventcode1(eCode1);
		del.setGroupCode(gCode);
		del.setScore1(aScore);
		del.setGroupLeader(gl);
		/*for (int x = 0; x < aList.size(); x++) {
			if (aList.get(x).getfirstname().equals(first)) {
				del.setfirstname(aList.get(x));
			}
		}*/
		datab.deleteAthlete(del);
	}
	
	public void editAthlete(int a, String first, String last, int age, String gender, String eCode1, String gCode, int aScore1, int aScore2, GroupLeader gl, DefaultTableModel modelAthletes){
		Athletes edit = aList.get(a);
		edit.setFirstName(first);
		edit.setLastName(last);
		edit.setAge(age);
		edit.setGender(gender);
		edit.setEventcode1(eCode1);
		edit.setGroupCode(gCode);
		edit.setScore1(aScore1);
		edit.setScore2(aScore2);
		edit.setGroupLeader(gl);
		/*for (int x = 0; x < aList.size(); x++) {
			if (aList.get(x).getfirstname().equals(first)) {
				del.setfirstname(aList.get(x));
			}
		}*/
		datab.updateAthlete(edit);
	}
	
	public Boolean validateAthlete( String first, String last, int age, String gender, String eCode1, String gCode, int aScore, GroupLeader gl){
		if(first!= null){
			if(last!=null){
				if(age<0){
					if(gender!= null){
						if(eCode1!= null){
							if(gCode!= null){
								if(aScore<0){
									if(gl!= null){
								
								return true;
								
							}
						}
					}
				}
			}
			
		}
	}
}
		return false;
	}
	
	/**
	 * This handles shows the Athletes in a table
	 * @param DefaultTableModel modelAthletes - used to add to the table
	 * @exception None
	 * @return None
	 */
	public void generateATable(DefaultTableModel modelAthletes) {
		for (int x = 0; x < aList.size(); x++) {
			String fName = aList.get(x).getFirstName();
			String lName = aList.get(x).getLastName();
			int age = aList.get(x).getAge();
			String gender = aList.get(x).getGender();
			String eCode1 = aList.get(x).getEventcode1();
			String eCode2 = aList.get(x).getEventcode2();
			String eName1 = null;
			String eName2 = null;
			for (int i = 0; i < eList.size(); i++) {
				if (eCode1.equals(eList.get(i).getEventCode())) {
					eName1 = eList.get(i).getEventName();
				}
				if (eCode2 != null && eCode2.equals(eList.get(i).getEventCode())) {
					eName2 = eList.get(i).getEventName();
				}
			}
			int score1 = aList.get(x).getScore1();
			int score2 = aList.get(x).getScore2();
			Object aData [] = {fName, lName, age, gender, eCode1, eName1, eCode2, eName2, score1, score2};
			modelAthletes.addRow(aData);
		}
	}

	public void generateEventsCBox(JComboBox<String> comboBoxEventName1) {
		String[] listEvents= new String[eList.size()];
		for (int x = 0; x < eList.size(); x++) {
			listEvents[x] = eList.get(x).getEventName();
		}
		comboBoxEventName1.setModel(new DefaultComboBoxModel<String>(listEvents));
	}
	
}
