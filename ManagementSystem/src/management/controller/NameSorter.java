package management.controller;

import java.util.Comparator;
import management.model.*;

public class NameSorter implements Comparator<Athletes> {

	public int compare(Athletes one, Athletes two) {
		return one.compareTo(two);
	}

}
