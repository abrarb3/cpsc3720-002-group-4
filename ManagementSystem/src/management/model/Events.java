package management.model;
import java.util.ArrayList;


public class Events {
	transient String id;
	private String EventName;			//name of the event
	private String EventCode;			//code of the event
	private String scoreunit;			//imperial unit of measure used (Distance, Time
	private int scoremax;				//maximum possible score
	private int scoremin;				//minimum possible score
	private int sortseq;				//order of display
	transient private ArrayList<Heats> listHeats = new ArrayList<Heats>(); //list of heats
	
	public Events(String eName, String eCode, String sUnit, int sMin, int sMax, int sSeq) {
		EventName = eName;
		EventCode = eCode;
		scoreunit = sUnit;
		scoremax = sMax;
		scoremin = sMin;
		sortseq = sSeq;
	}

	public String getEventName() {
		return EventName;
	}

	public void setEventName(String eventName) {
		EventName = eventName;
	}

	public String getEventCode() {
		return EventCode;
	}

	public void setEventCode(String eventCode) {
		EventCode = eventCode;
	}

	public String getScoreunit() {
		return scoreunit;
	}

	public void setScoreunit(String scoreunit) {
		this.scoreunit = scoreunit;
	}

	public int getScoremax() {
		return scoremax;
	}

	public void setScoremax(int scoremax) {
		this.scoremax = scoremax;
	}

	public int getScoremin() {
		return scoremin;
	}

	public void setScoremin(int scoremin) {
		this.scoremin = scoremin;
	}

	public int getSortseq() {
		return sortseq;
	}

	public void setSortseq(int sortseq) {
		this.sortseq = sortseq;
	}

	public ArrayList<Heats> getListHeats() {
		return listHeats;
	}

	public void setListHeats(ArrayList<Heats> listHeats) {
		this.listHeats = listHeats;
	}
	
	public String toString() {
		return EventName + listHeats;
	}

	public void setId(String id) {
		id = this.id;
	}

	public String getId() {
		return id;
	}
}
