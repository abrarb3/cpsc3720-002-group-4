/**
 * This is the Athletes class for modeling its desired properties in the database.
 * @param None
 * @exception None
 * @return None
 */

package management.model;


public class Athletes implements Comparable<Athletes>{
	transient String id;
	private String FirstName;			//first name
	private String LastName;			//last name
	private String groupCode;			//code for the group
	private String eventcode1;			//code for the event 1
	private String eventcode2;			//code for the event 2
	private String gender;				//athlete's gender
	private int age;					//athlete's age
	private int score1;					//athlete's qualifying score
	private int score2;
	transient private GroupLeader gl;
	
	public Athletes(String first, String last, int age, String gender, String eCode1, String gCode, int aScore1, GroupLeader gl) {
		FirstName = first;
		LastName = last;
		groupCode = gCode;
		setEventcode1(eCode1);
		this.gender = gender;
		this.age = age;
		this.setGroupLeader(gl);
		setScore1(aScore1);
	}

	public void setId(String id) {
		id = this.id;
	}

	public int getScore1() {
		return score1;
	}
	
	public int getScore2() {
		return score2;
	}

	public void setScore1(int score) {
		this.score1 = score;
	}
	
	public void setScore2(int score) {
		this.score2 = score;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public GroupLeader getGroupLeader() {
		return gl;
	}

	public void setGroupLeader(GroupLeader gl) {
		this.gl = gl;
	}

	public int compareTo(Athletes other) {
		int score = 0, oScore = 0;
		if (eventcode1.equals(other.getEventcode1())){
			score = score1;
			oScore = other.getScore1();
		}
		else if (eventcode1.equals(other.getEventcode2())){
			score = score1;
			oScore = other.getScore2();
		}
		else if (eventcode2.equals(other.getEventcode1())) {
			score = score2;
			oScore = other.getScore1();
		}
		else {
			score = score2;
			oScore = other.getScore2();
		}
		int c = 0;
		c = Integer.compare(score, oScore);
		if (c == 0) {
			c = LastName.compareTo(other.getLastName());
		}
		return c;
	}

	public String toString1() {
		return (String.format("%-26s %2s %2s", LastName+", "+FirstName, age, gender));
	}
	
	public String toString2() {
		String school = gl.getSchoolName().getSchoolname().replace("\"", "");
		String name = gl.getGroupLeaderName().replace("\"", "");
		if (school.length() > 17) {
			school = school.substring(0, 16);
		}
		if (name.length() > 12){
			name = name.substring(0, 12);
		}
		return (String.format("%5s %-16s %-16s", gl.getGroupCode(), school, name));
	}
	
	public String printScore(String type, String eventCode) {
		int score;
		if (eventCode.equals(eventcode1)){
			score = score1;
		}
		else{
			score = score2;
		}
		if (score == 0){
			return "    ";
		}
		String theScore = Integer.toString(score);
		String scoreR;
		if (type.equals("T")) {
			if (theScore.length() == 2){
				scoreR = "0:"+theScore;
			}
			else if (theScore.length()==1) {
				scoreR = "0:0"+theScore;
			}
			else {
				scoreR = theScore.substring(0, theScore.length()-1) + ":" +theScore.substring(theScore.length()-1);
			}
			return scoreR;
		}
		else {
			if (theScore.length() < 4) {
				for (int x = 0; x < 4-theScore.length(); x++) {
					theScore = "0"+theScore;
				}
			}
			return theScore.substring(0, 2) +"' "+ theScore.substring(2)+"\""; 
		}
	}

	public String getEventcode1() {
		return eventcode1;
	}

	public void setEventcode1(String eventcode1) {
		this.eventcode1 = eventcode1;
	}

	public String getEventcode2() {
		return eventcode2;
	}

	public void setEventcode2(String eventcode2) {
		this.eventcode2 = eventcode2;
	}

	public String getId() {
		return id;
	}
}
