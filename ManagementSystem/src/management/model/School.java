package management.model;
import java.util.ArrayList;


public class School {
	transient String id;
	private String groupcode;
	private String schoolname;
	transient private ArrayList<GroupLeader> listGroupLeaders = new ArrayList<GroupLeader>();
	
	public School(String groupcode, String schoolname) {
		this.groupcode = groupcode;
		this.schoolname = schoolname;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getGroupcode() {
		return groupcode;
	}
	public void setGroupcode(String groupcode) {
		this.groupcode = groupcode;
	}
	public String getSchoolname() {
		return schoolname;
	}
	public void setSchoolname(String schoolname) {
		this.schoolname = schoolname;
	}
	public ArrayList<GroupLeader> getListGroupLeaders() {
		return listGroupLeaders;
	}
	public void setListGroupLeaders(ArrayList<GroupLeader> listGroupLeaders) {
		this.listGroupLeaders = listGroupLeaders;
	}

}
