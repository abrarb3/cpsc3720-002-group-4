package management.model;

public class GroupLeader {
	transient String id;
	private String groupcode;					//code for the group
	private String GroupLeaderName;				//name of the group leader
	transient private School schoolName;
	
	public GroupLeader(String code, School schoolName, String name) {
		groupcode = code;
		GroupLeaderName = name;
		this.schoolName = schoolName;
	}
	public String getGroupCode() {
		return groupcode;
	}

	public void setGroupCode(String groupCode) {
		this.groupcode = groupCode;
	}

	public String getGroupLeaderName() {
		return GroupLeaderName;
	}

	public void setGroupLeaderName(String groupLeaderName) {
		this.GroupLeaderName = groupLeaderName;
	}

	public School getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(School schoolName) {
		this.schoolName = schoolName;
	}

	/*public ArrayList<Athletes> getListAthletes() {
		return listAthletes;
	}

	public void setListAthletes(ArrayList<Athletes> listAthletes) {
		this.listAthletes = listAthletes;
	}
	public String toString() {
		return GroupLeaderName + " " +groupcode+schoolName.getSchoolname()+listAthletes;
	}*/
	public void setId(String id) {
		id = this.id;		
	}
}
