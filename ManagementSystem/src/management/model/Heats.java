package management.model;
import java.util.ArrayList;


public class Heats implements Comparable<Heats>{
	transient String id;
	private String EventCode;			//code of the event
	private String gender;				//gender for the heat
	private String time;				//time of the event
	private int maxage;					//maximum age for the event
	private int minage;					//minimum age for the event
	private int numheats;				//number of heats
	private Events EventName;
	transient private ArrayList<Athletes> listAthletes = new ArrayList<Athletes>();
	
	public Heats(String eCode, String eGender, int miAge, int mAge, String eTime, int nHeats, Events eventName) {
		EventCode = eCode;
		gender = eGender;
		time = eTime;
		maxage = mAge;
		minage = miAge;
		numheats = nHeats;
		this.EventName = eventName;
	}

	public String getEventCode() {
		return EventCode;
	}

	public void setEventCode(String eventCode) {
		this.EventCode = eventCode;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public int getMaxAge() {
		return maxage;
	}

	public void setMaxAge(int maxAge) {
		this.maxage = maxAge;
	}

	public int getMinAge() {
		return minage;
	}

	public void setMinAge(int minAge) {
		this.minage = minAge;
	}

	public int getNumHeats() {
		return numheats;
	}

	public void setNumHeats(int numHeats) {
		this.numheats = numHeats;
	}
	public String toString() {
		return EventCode+gender;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ArrayList<Athletes> getListAthletes() {
		return listAthletes;
	}

	public void setListAthletes(ArrayList<Athletes> listAthletes) {
		this.listAthletes = listAthletes;
	}

	public Events getEventName() {
		return EventName;
	}

	public void setEventName(Events eventName) {
		this.EventName = eventName;
	}

	@Override
	public int compareTo(Heats other) {
		return Integer.compare(EventName.getSortseq(),(other.getEventName().getSortseq()));
	}
	
	public boolean compareHeat(Heats other) { 
		if (EventCode.equals(other.getEventCode()) && gender.equals(other.getGender()) && time.equals(other.getTime()) && maxage == other.getMaxAge() && minage == other.getMinAge() && numheats == other.getNumHeats())
			return true;
		return false;
	}

	public String getId() {
		return id;
	}
}
