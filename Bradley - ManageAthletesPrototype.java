/*import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JButton;
import javax.swing.JList;
import java.awt.BorderLayout;


public class ManageEventsPrototype extends JFrame {

	
	  //Create the panel.
	 
	public ManageEventsPrototype() {
		setTitle("Manage Events Prototype");
		getContentPane().setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 450, 21);
		getContentPane().add(menuBar);
		
		JMenu mnNewMenu = new JMenu("File");
		menuBar.add(mnNewMenu);
	}

}*/

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

@SuppressWarnings("serial")
public class ManageAthletesPrototype extends JFrame {

	private int currentI;
	private JScrollPane heatPane;
	private JTable issueTable;
	private DefaultTableModel modelIssues;
	private JPanel contentPane;
	private JTextField textFieldID;
	private JTextField textFieldDescription;
	private JComboBox comboBoxStatus;
	private JPanel buttonPanel;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ManageAthletesPrototype frame = new ManageAthletesPrototype();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ManageAthletesPrototype() {
		setTitle("Project Management System");
		setAlwaysOnTop(true);
		JFrame frame = new JFrame("Project Management System");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 692, 421);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(1, 0, 1, 2));
		setContentPane(contentPane);
				
		contentPane.setLayout(null);
		
		JLabel lblIssues = new JLabel("Athlete Information");
		lblIssues.setHorizontalAlignment(SwingConstants.CENTER);
		lblIssues.setBounds(144, 0, 444, 27);
		lblIssues.setFont(new Font("Tahoma", Font.PLAIN, 12));
		contentPane.add(lblIssues);		
		
		/*DefaultTableModel modelProjects = new DefaultTableModel(new Object[][] {}, new Object[] {"Project Names"});
		Object[] projObj1= {"Project One"};
		modelProjects.addRow(projObj1);
		Object[] projObj2= {"Project Two"};
		modelProjects.addRow(projObj2);
		Object[] projObj3= {"Project Three"};
		modelProjects.addRow(projObj3);*/
		
		heatPane = new JScrollPane();
		heatPane.setBounds(10, 22, 656, 150);
		contentPane.add(heatPane);
		
		modelIssues = new DefaultTableModel(new Object[][] {}, new Object[] {"Athlete Name", "Gender", "BirthDate", "Event#", "Qualifying Score"});
		modelIssues.addRow(new Object []{"Alex Jones", "M", "02/02/02", "1", "2:30"});
		modelIssues.addRow(new Object []{"John Mays", "M", "01/02/04", "1", "3:30"});
		modelIssues.addRow(new Object []{"Jane Jacks", "F", "02/02/05", "1", "1:30"});
		issueTable = new JTable(modelIssues);
		heatPane.setViewportView(issueTable);
		issueTable.setBorder(new LineBorder(new Color(0, 0, 0)));
		/*selectionModel.addListSelectionListener(new ListSelectionListener(){

			@Override
			public void valueChanged(ListSelectionEvent e) {
				// TODO Auto-generated method stub
				
			}
		
		});*/
		
		buttonPanel = new JPanel();
		buttonPanel.setBounds(144, 184, 471, 27);
		contentPane.add(buttonPanel);
		buttonPanel.setLayout(null);
		
		JButton btnNewButton = new JButton("Create");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//stuff
			}
		});
		btnNewButton.setBounds(44, 0, 89, 23);
		buttonPanel.add(btnNewButton);
		
		JButton btnEdit = new JButton("Edit");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		btnEdit.setBounds(177, 0, 89, 23);
		buttonPanel.add(btnEdit);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (issueTable.getSelectedRow() != -1){
					modelIssues.removeRow(issueTable.getSelectedRow());
				}
			}
		});
		btnDelete.setBounds(310, 0, 89, 23);
		buttonPanel.add(btnDelete);
		
		JPanel dataPanel = new JPanel();
		dataPanel.setBounds(144, 210, 471, 146);
		contentPane.add(dataPanel);
		dataPanel.setLayout(null);
		
		JLabel lblAth = new JLabel("Athlete Name");
		lblAth.setBounds(10, 15, 81, 14);
		dataPanel.add(lblAth);
		
		JLabel lblGender = new JLabel("Gender");
		lblGender.setBounds(10, 44, 46, 14);
		dataPanel.add(lblGender);
		
		JLabel lblDescription = new JLabel("BirthDate");
		lblDescription.setBounds(10, 73, 81, 14);
		dataPanel.add(lblDescription);
		
		JLabel lblStatus = new JLabel("Event#");
		lblStatus.setBounds(10, 102, 81, 14);
		dataPanel.add(lblStatus);
		
		JLabel lblQsc = new JLabel("Qualifying Score");
		lblQsc.setBounds(10, 131, 106, 14);
		dataPanel.add(lblQsc);
		
		textFieldID = new JTextField();
		textFieldID.setBounds(126, 13, 306, 20);
		dataPanel.add(textFieldID);
		textFieldID.setColumns(10);
		
		textFieldDescription = new JTextField();
		textFieldDescription.setBounds(126, 67, 306, 20);
		dataPanel.add(textFieldDescription);
		textFieldDescription.setColumns(10);
		
		comboBoxStatus = new JComboBox();
		comboBoxStatus.setModel(new DefaultComboBoxModel(new String[] {" ","1", "2", "3", "4","5","6","7","8","9","10","11","12","13"}));
		comboBoxStatus.setBounds(126, 96, 255, 20);
		dataPanel.add(comboBoxStatus);
		
		textField = new JTextField();
		textField.setBounds(126, 125, 306, 20);
		dataPanel.add(textField);
		textField.setColumns(10);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {" ","Male", "Female"}));
		comboBox.setBounds(126, 41, 89, 20);
		dataPanel.add(comboBox);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 69, 21);
		contentPane.add(menuBar);
		
		JMenu mnFile = new JMenu("Menu");
		menuBar.add(mnFile);
		
		JMenuItem mntmAthletes = new JMenuItem("Athletes");
		mnFile.add(mntmAthletes);
		
	}
}